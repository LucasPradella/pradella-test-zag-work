package br.com.pradella.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.pradella.entity.Cliente;
import br.com.pradella.repository.ClienteRepo;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepo repo;
	

	public Cliente processSave(Cliente cliente) {
     	repo.save(defined(cliente));
		return cliente;
	}

	public void delete(String id) {
		repo.deleteById(id);
	}
	
	public Page<Cliente> findAll(int page, int count) {
		Pageable pages = new PageRequest(page, count);
		return repo.findAll(pages);
	}
	
	public List<Cliente> findAll() {
		return repo.findAll();
	}
	
	
	
	
	
	private Cliente defined(Cliente cliente) {
		switch (cliente.getRisc().get(0)) {
		case A: {
			cliente.setInterestRate(0);
			return cliente;
		}
		case B: {
			cliente.setInterestRate(10);
			return cliente;
		}
		case C: {
			cliente.setInterestRate(20);
			return cliente;
		}
		default: return cliente;
     }
	}







}
