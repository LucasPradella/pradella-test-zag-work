package br.com.pradella.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.pradella.entity.Cliente;

public interface ClienteRepo extends MongoRepository<Cliente, String> {
	
}
