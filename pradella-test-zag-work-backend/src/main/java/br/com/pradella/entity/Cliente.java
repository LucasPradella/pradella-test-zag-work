package br.com.pradella.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.com.pradella.enuns.RiscEnum;

@Document
public class Cliente {

	@Id
	private String id;

	@Valid
	@NotNull(message = "name requerido")
	private String name;

	@NotNull(message = "limit requerido")
	private double limit;

	@NotEmpty(message = "risc requerido")
	private List<RiscEnum> risc;

	private int interestRate;

	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLimit() {
		return limit;
	}

	public void setLimit(double limit) {
		this.limit = limit;
	}

	public List<RiscEnum> getRisc() {
		return risc;
	}

	public void setRisc(List<RiscEnum> risc) {
		this.risc = risc;
	}

	public int getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(int interestRate) {
		this.interestRate = interestRate;
	}

}
