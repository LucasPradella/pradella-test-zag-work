package br.com.pradella;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PradellaTestZagWorkBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PradellaTestZagWorkBackendApplication.class, args);
	}

}

