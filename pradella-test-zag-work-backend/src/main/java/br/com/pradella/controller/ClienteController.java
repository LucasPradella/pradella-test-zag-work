package br.com.pradella.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pradella.controller.response.Response;
import br.com.pradella.entity.Cliente;
import br.com.pradella.service.ClienteService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	
	
	@PostMapping(value = "/cliente")
	public ResponseEntity<Response<Cliente>> save(HttpServletRequest request, @RequestBody Cliente client, BindingResult result) {
		Response<Cliente> response = new Response<Cliente>();

		if (StringUtils.isEmpty(client.getName())) {
			response.getErrors().add("Name is requerid ");
			return ResponseEntity.badRequest().body(response);
		}
		
		if (client.getRisc() == null) {
			response.getErrors().add("Risc is requerid ");
			return ResponseEntity.badRequest().body(response);
		}
		
		if (client.getLimit() == 0.0) {
			response.getErrors().add("Limit is requerid ");
			return ResponseEntity.badRequest().body(response);
		}
		

		Cliente processSave = clienteService.processSave(client);
		response.setData(processSave);
		return ResponseEntity.ok(response);
	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Response<String>> deleteById(@PathVariable("id") String id) {
		clienteService.delete(id);
		return ResponseEntity.ok(new Response<String>());
	}		
		
	@GetMapping(value = {"{page}/{count}"})
	public ResponseEntity<Response<Page<Cliente>>> findAll(@PathVariable int page, @PathVariable int count) {
		Response<Page<Cliente>> response = new Response<Page<Cliente>>();
		Page<Cliente> users = clienteService.findAll(page, count);
		response.setData(users);
		return ResponseEntity.ok(response);
	}	
	
	@GetMapping(value = {""})
	public ResponseEntity<Response<List<Cliente>>> findAll() {
		Response<List<Cliente>> response = new Response<List<Cliente>>();
		List<Cliente> users = clienteService.findAll();
		response.setData(users);
		return ResponseEntity.ok(response);
	}	
	

}
