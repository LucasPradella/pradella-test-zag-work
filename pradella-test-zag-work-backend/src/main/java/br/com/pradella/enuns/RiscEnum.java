package br.com.pradella.enuns;


public enum RiscEnum {

	A,B,C;

	public static RiscEnum getStatus(String risc) {
		switch (risc) {
		case "a": return A; 
		case "b": return B;
		case "c": return C;
		default: return A;
		}

	}
}
