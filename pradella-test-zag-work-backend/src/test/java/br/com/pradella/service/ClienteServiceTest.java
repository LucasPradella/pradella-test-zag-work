package br.com.pradella.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import br.com.pradella.entity.Cliente;
import br.com.pradella.enuns.RiscEnum;
import br.com.pradella.repository.ClienteRepo;

public class ClienteServiceTest {

	@InjectMocks
	private ClienteService clienteService;

	@Mock
	private ClienteRepo repo;

	@Before
	public void setUp() throws Exception {
		initMocks(this);
	}

	@Test
	public void testInterestRate0() {
		Cliente cliente = clienteService.processSave(clienteA());

		verify(repo).save(cliente);
		assertEquals("Tony Stark", cliente.getName());
		assertEquals(Arrays.asList(RiscEnum.getStatus("a")), cliente.getRisc());
		assertEquals(100.00, cliente.getLimit(), 0.2);
		assertEquals(0, cliente.getInterestRate());
	}

	@Test
	public void testInterestRate10() {
		Cliente cliente = clienteService.processSave(clienteB());

		assertEquals(10, cliente.getInterestRate());
	}

	@Test
	public void testInterestRate20() {
		Cliente cliente = clienteService.processSave(clienteC());

		assertEquals(20, cliente.getInterestRate());
	}

	@Test
	public void dele() {
		clienteService.delete("id");

		verify(repo).deleteById("id");
	}


	
	
	
	
	private Cliente clienteA() {
		Cliente cliente = new Cliente();
		cliente.setName("Tony Stark");
		cliente.setRisc(Arrays.asList(RiscEnum.getStatus("a")));
		cliente.setLimit(100.00);

		return cliente;
	}

	private Cliente clienteB() {
		Cliente cliente = new Cliente();
		cliente.setName("Tony Stark");
		cliente.setRisc(Arrays.asList(RiscEnum.getStatus("b")));
		cliente.setLimit(100.00);

		return cliente;
	}

	private Cliente clienteC() {
		Cliente cliente = new Cliente();
		cliente.setName("Tony Stark");
		cliente.setRisc(Arrays.asList(RiscEnum.getStatus("c")));
		cliente.setLimit(100.00);

		return cliente;
	}

}
