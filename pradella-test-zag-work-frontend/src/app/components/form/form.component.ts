import { Client } from './../../core/models/client.model';
import { ClientService } from './../../core/services/client.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router} from "@angular/router";

import { NgForm } from '@angular/forms';
import { ResponseApi } from 'src/app/core/models/response-api';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  
  private client = new Client('','',null,null, 0);


  constructor(
    private clientService: ClientService,
    private routes: Router,
  ) { }

  @ViewChild("form")
  form: NgForm;
  
  ngOnInit() {
  }

  register() {
      this.client.risc = [this.client.risc[0]]; 
      this.routes.navigate(['']);
      this.clientService.save(this.client).subscribe((responseApi: ResponseApi) => {
        this.client = new Client('','',0,null, 0);
        let ret: Client = responseApi.data;
        console.log('Cliente salvo -> ', ret)
        this.form.resetForm();
        this.routes.navigate(['list']);
      });
  }

}
