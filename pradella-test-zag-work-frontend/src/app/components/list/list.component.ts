import { ClientService } from './../../core/services/client.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ResponseApi } from 'src/app/core/models/response-api';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  listCliente = [];
  displayedColumns: string[] = ['name', 'limit', 'risc', 'tax', 'delete'];
  dataSource = new MatTableDataSource<PeriodicElement>(this.listCliente);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private clientService: ClientService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.findAll();
  }

  findAll() {
    this.clientService.findAll().subscribe((responseApi: ResponseApi) => {
      this.listCliente = responseApi['data'];
      this.dataSource = new MatTableDataSource<PeriodicElement>(this.listCliente);
    });
  }

  delete(id: string) {
    this.clientService.delete(id).subscribe((responseApi: ResponseApi) => {
      this.findAll();
    });
  }

}

export interface PeriodicElement {
  name: string;
}

