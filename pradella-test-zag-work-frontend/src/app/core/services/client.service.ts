import { Injectable } from '@angular/core';



import { Client } from '../models/client.model';
import { API_TEST_ZAG_WORK } from './api-test-zag-work';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }


  save(client: Client) {
    client.id = null;
    return this.http.post(`${API_TEST_ZAG_WORK}/api/cliente`, client);
  }

  findAllPages(page: number, count: number) {
    return this.http.get(`${API_TEST_ZAG_WORK}/api/${page}/${count}`);
  }

  findAll() {
    return this.http.get(`${API_TEST_ZAG_WORK}/api/`);
  }

  delete(id: string) {
    return this.http.delete(`${API_TEST_ZAG_WORK}/api/${id}`);
  }

}
