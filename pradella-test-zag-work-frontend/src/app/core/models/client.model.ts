export class Client {

    constructor(    
        public id: string,
        public name: string,
        public limit: number,
        public risc: Array<string>,
        public interestRate: number,
    ){
        
    }
}
