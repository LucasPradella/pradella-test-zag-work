import {Routes, RouterModule} from '@angular/router'
import { ModuleWithProviders } from '@angular/core';

import { FormComponent } from './components/form/form.component';
import { ListComponent } from './components/list/list.component';

export const ROUTES: Routes = [
    // { path : '', component: FormComponent },
    { path : 'list', component: ListComponent },
    

]

export const routes: ModuleWithProviders = RouterModule.forRoot(ROUTES);